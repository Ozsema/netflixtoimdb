var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var unirest = require('unirest');
var mongoose    = require("mongoose");

mongoose.connect("mongodb://guldamlaozsema:adana91@ds061196.mlab.com:61196/yelpcamp");

var netflixSchema = mongoose.Schema({
    title: String,
    image: String,
    rating: String
});

var Netflix = mongoose.model("Netflix", netflixSchema)

"use strict";
 
let uNoGS = require("unogs"),
    unogs = new uNoGS("token");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use( express.static( "public" ) );

// These code snippets use an open-source library. http://unirest.io/nodejs
app.get("/list", function(req,res){
    res.render("list");
});

app.get("/", function(req,res){
  
    Netflix.find({}, function(err, allnetflix){
       if(err){
           console.log(err);
       } else {
           res.render("bootstrap", {data: allnetflix})
       }
    });
    
});

app.get("/alphabetic", function(req,res){
  
    Netflix.find({}, null, {sort: {title: 1}}, function(err, allnetflix){
       if(err){
           console.log(err);
       } else {
          
              res.render("bootstrap", {data: allnetflix})
       }
    });
    
});


app.get("/rating", function(req,res){
  
    Netflix.find({}, null, {sort: {rating: -1}}, function(err, allnetflix){
       if(err){
           console.log(err);
       } else {
          res.render("bootstrap", {data: allnetflix})
       }
    });
    
});


//app.get("/", function(req,res){
   
  // for(var i = 0; i < 200 ; i++){ 
       
//      unirest.get("https://unogs-unogs-v1.p.mashape.com/aaapi.cgi?q={query}-!1900,2018-!0,5-!0,10-!Any-!Any-!Any-!Any-!gt1-!{downloadable}&t=ns&cl=432&st=adv&ob=Relevance&p=" + i + "&sa=and")
//.header("X-Mashape-Key", "v0cX8NQigomshNIGRKnHMSzolHeZp1wvuZYjsn2BEBEa4NcAAW")
//.header("Accept", "application/json")
//.end(function (result) {
   
  //    for ( var key in result.body.ITEMS) {
    //         Netflix.create({
      //  title: result.body.ITEMS[key]["title"],
    //    image: result.body.ITEMS[key]["image"],
     //   rating: result.body.ITEMS[key]["rating"]
    //}, function(err, netflix){
      // if(err){
        //   console.log(err);
 //      }
  //  });         
                    
    //     }
//});

//}

//res.render("list");

//})

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("server has started");
});